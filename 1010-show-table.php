<?php

$pdo = new PDO('mysql:host=localhost;dbname=new;charset=utf8', 'root', '');

$tableName = !empty($_GET['table']) ? $_GET['table'] : "";

if (!empty($_POST)) {

    $changedField = !empty($_POST['field']) ? $_POST['field'] : "";

    if ($_POST['newType'] !== 'новый тип') {

        $newType = $_POST['newType'];

    } else {

        $newType = "";

    }

    $newName = !empty($_POST['newName']) ? $_POST['newName'] : "";

    $del = !empty($_POST['delete']) ? $_POST['delete'] : "";

    $newFieldName = !empty($_POST['newFieldName']) ? $_POST['newFieldName'] : "";

    $newFieldType = !empty($_POST['newFieldType']) ? $_POST['newFieldType'] : "";
}

if ($newType !== "" && $newName == "" && $del == '') {

    $inquireType = "ALTER TABLE $tableName MODIFY $changedField $newType NOT NULL";

    $sthType=$pdo->prepare("$inquireType");

    $sthType->execute();
}

if ($newName !== "" && $newType !== "" && $del == '') {

    $inquireName = "ALTER TABLE $tableName CHANGE $changedField $newName $newType";

    $sthName=$pdo->prepare("$inquireName");

    $sthName->execute();
}

if ($del == 'on') {

    $inquireDel = "ALTER TABLE $tableName DROP COLUMN $changedField";

    $sthDel=$pdo->prepare("$inquireDel");

    $sthDel->execute();
}

if ($newFieldName !== "" && $newFieldType !== "") {

    $inquireCreateField = "ALTER TABLE $tableName ADD $newFieldName $newFieldType";

    $sthCreateField=$pdo->prepare($inquireCreateField);

    $sthCreateField->execute();
}



$inquire = "DESCRIBE $tableName";

$sth=$pdo->prepare("$inquire");

$sth->execute();

$result=$sth->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <title>start</title>
</head>
<body>

<p>Структура таблицы <?php echo $tableName ?></p>
<table>
    <tr>
        <td>Имя поля</td>
        <td>Тип поля</td>
    </tr>
    <?php foreach ($result as $k => $v) : ?>
        <tr>
            <td><?php echo $v['Field']; ?></td>

            <td><?php echo $v['Type']; ?></td>
        </tr>
    <?php endforeach ?>
</table>

    <form action="" method="post">
            <legend>
                <p>Для внесения изменений в структуру таблицы нужно выбрать имя 
                поля в которое необходимо внести изменения</p>
                <p>Для изменении имени поля, нужно также
                обязательно выбрать тип поля.</p>
                <p>Для изменении типа поля, нужно выбрать только
                новый тип поля.</p>
                <p>Если выбрать удалить поле - поле будет удалено.</p>
            </legend>

                <select size="1" name="field">
                    <?php foreach ($result as $k => $v) : ?>
                        <option><?php echo $v['Field']; ?></option>
                    <?php endforeach ?>
                </select>

            <select size="1" name="newType">
                <option value="новый тип">новый тип</option>
                <option value="INT">INT</option>
                <option value="VARCHAR(50)">VARCHAR</option>
                <option value="TIMESTAMP">TIMESTAMP</option>
                <option value="FLOAT">FLOAT</option>
            </select>

            <input type="text" placeholder="новое название" name="newName">

            <label><input type="checkbox" name="delete">удалить поле</label>

            <input type="submit" value="внести изменения">
    </form>

    <form action="" method="post">

        <legend>
            <p>Добавить поле втаблицу</p>
        </legend>

        <input type="text" placeholder="название нового поля" name="newFieldName">

        <select size="1" name="newFieldType">

            <option value="INT">INT</option>
            <option value="VARCHAR(50)">VARCHAR</option>
            <option value="TIMESTAMP">TIMESTAMP</option>
            <option value="FLOAT">FLOAT</option>
            <input type="submit" value="Создать новое поле">

        </select>

    </form>

    <p><a href="./1010-show-db.php">посмотреть все таблицы</a></p>

</table>

</body>
</html>