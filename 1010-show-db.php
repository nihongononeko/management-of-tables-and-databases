<?php

$pdo = new PDO('mysql:host=localhost;dbname=new;charset=utf8', 'root', '');

if (!empty($_POST)) {

    $nameNewTable = !empty($_POST['tableName']) ? $_POST['tableName'] : "";

    echo "$nameNewTable <br>";

    $n = "CREATE TABLE $nameNewTable (`id` int NOT NULL AUTO_INCREMENT, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
    
    $cc=$pdo->prepare($n);
    
    $cc->execute();
}


if (!empty($_GET)) {

    $deletedTable = $_GET['table'];

    $inquireDelTable = "DROP TABLE $deletedTable";

    $sthDelTable=$pdo->prepare("$inquireDelTable");

    $sthDelTable->execute();
}

$sth=$pdo->prepare("SHOW TABLES FROM new");

$sth->execute();

$result=$sth->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>start</title>
</head>
<body>

<p>Таблицы базыданных</p>

<table>

    <?php foreach ($result as $arr) : ?>

        <tr>

            <?php foreach ($arr as $table) : ?>

                <td>
                    <a href="./1010-show-table.php?table=<?php echo $table ?>"><?php echo $table ?></a>
                    <a href="1010-show-db.php?table=<?php echo $table ?>">удалить таблицу</a>
                </td>

            <?php endforeach ?>

        </tr>

    <?php endforeach ?>

</table>

<p>Создать новую таблицу</p>
<p>Введите название новой таблицы.</p>

<form action="" method="post">

    <input type="text" placeholder="Название новой таблицы" name="tableName">

    <input type="submit" value="создать новую таблицу">

</form>

</body>
</html>